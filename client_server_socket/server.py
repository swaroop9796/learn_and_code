import socket

HOST = 'localhost'  # Standard loopback interface address (localhost)
PORT = 8889    # Port to listen on (non-privileged ports are > 1023)

socket_object = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket_object.bind((HOST, PORT))
print("socket binded to", PORT)
socket_object.listen(3)
print("socket is listening")

while True:
        connection, address = socket_object.accept()
        print('Connected by', address)
        with connection:
            data = connection.recv(1024)
            print("Received from client- ",data.decode("utf-8"))
            connection.sendall(b"Server acknowledgement")