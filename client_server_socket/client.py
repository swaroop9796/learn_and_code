import socket

HOST = 'localhost'  # The server's hostname or IP address
PORT = 8889     # The port used by the server

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket:
    socket.connect((HOST, PORT))
    socket.sendall(b'Hello, world')
    data = socket.recv(1024)

print('Received-', data.decode("utf-8"))