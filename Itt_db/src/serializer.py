import pickle

def insert_object_to_file(object, file_name):
    pickle.dump(object, file_name, pickle.HIGHEST_PROTOCOL)

def retrieve_object_from_file(file_name):
    return pickle.load(file_name)