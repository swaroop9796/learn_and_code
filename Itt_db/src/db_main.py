import sys
import os
sys.path.append(os.path.dirname(os.getcwd()))
from user_interaction import *

if __name__=="__main__":
    while(1):
        print("1.Insert record\n2.Search record\n3.Display_all records\n4.Delete\n5.Modify\n6.Exit")
        choice = int(input("Enter:"))
        if choice is 1:
            insert()
        if choice is 2:
            search()
        if choice is 3:
            display()
        if choice is 4:
            delete()
        if choice is 5:
            modify()
        if choice is 6:
            break