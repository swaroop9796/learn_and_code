from src.serializer import *

class Db_transactions():
    def get_id(self):
        list_for_id = list(self.unpickle_database())
        try:
            last_id = int(list_for_id[-1].id)
            required_id = last_id+1
        except (AttributeError, IndexError, TypeError):
            required_id = 1
        finally:
            return required_id


    def insert_into_file(self):
        try:
            file_name = type(self).__name__ + '.txt'
            with open(file_name, 'ab') as input_file:
                self.id = self.get_id()
                insert_object_to_file(self, input_file)
        except Exceptions as e:
            print("An error occurred:",e)

    def unpickle_database(self):
        file_name = type(self).__name__ + '.txt'
        try:
            with open(file_name, 'rb') as output_file:
                while True:
                    try:
                        yield retrieve_object_from_file(output_file)
                    except EOFError:
                        break
        except FileNotFoundError:
            print("There is no file associated with", type(self).__name__, "object")

    def display_all_records(self):
        file_name = type(self).__name__ + '.txt'
        count = 0
        try:
            with open(file_name, 'rb') as output_file:
                while True:
                    try:
                        record = retrieve_object_from_file(output_file)
                        count +=1
                        if type(self).__name__ == "Customers":
                            print(record.id,'-',record.name,'-',record.department)
                        else:
                            print(record.id,'-',record.name,'-',record.project)
                    except EOFError:
                        if count == 0:
                            print("There are no records found")
                        break
        except FileNotFoundError:
            print("There is no file associated with",type(self).__name__ ,"object")

    def search_records(self):
        flag = False
        try:
            file_name = type(self).__name__ + '.txt'
            with open(file_name, 'rb') as output_file:
                while True:
                    try:
                        record = retrieve_object_from_file(output_file)
                        if record.name == self.name:
                            if type(self).__name__ == "Customers":
                                print(record.id,'-',record.name,'-',record.department)
                            else:
                                print(record.id,'-',record.name,'-',record.project)
                            flag=True
                    except EOFError:
                        return flag
        except FileNotFoundError:
            print("There is no file associated with",type(self).__name__ ,'object')

    def truncate_file(self):
        file_name = type(self).__name__ + '.txt'
        with open(file_name, 'r+') as input_file:
            input_file.truncate()

    def save_object(self, obj):
        file_name = type(self).__name__ + '.txt'
        with open(file_name, 'ab') as input_file:
            insert_object_to_file(obj, input_file)

    def get_no_of_matching_objects(self, record_objs):
        obj_count = 0
        for record_obj in record_objs:
            if self.name == record_obj.name:
                obj_count+=1
        return obj_count

    def delete_records(self):
        record_objs = list(self.unpickle_database())
        no_of_matching_records = self.get_no_of_matching_objects(record_objs)
        if no_of_matching_records == 1:
            for record_obj in record_objs:
                if record_obj.name == self.name:
                    del record_objs[record_objs.index(record_obj)]
                    print("1 record deleted")
                    break
        elif no_of_matching_records>1:
            print("These are the records that match your input:\n")
            self.search_records()
            return False
        elif no_of_matching_records == 0:
            print("No matching records found")
            return True
        self.truncate_file()
        for record_obj in record_objs:
            self.save_object(record_obj)
            return True

    def delete_single_record(self):
        record_objs = list(self.unpickle_database())
        for record_obj in record_objs:
            if record_obj.id == self.id:
                del record_objs[record_objs.index(record_obj)]
        self.truncate_file()
        for record_obj in record_objs:
            self.save_object(record_obj)

    def delete_all_records(self):
        record_objs = list(self.unpickle_database())
        for _ in range(len(record_objs)):
            for record_obj in record_objs:
                if record_obj.name == self.name:
                    del record_objs[record_objs.index(record_obj)]
        self.truncate_file()
        for record_obj in record_objs:
            self.save_object(record_obj)

    def modify_single_record(self, records, to_be_modified_field, modified_attribute):
        for record in records:
            if record.name == self.name:
                setattr(record, to_be_modified_field, modified_attribute)
                break
        self.truncate_file()
        for record in records:
            self.save_object(record)

    def modify_record_using_id(self, records, record_id, to_be_modified_field, modified_attribute):
        for record in records:
            if record_id == record.id:
                setattr(record, to_be_modified_field, modified_attribute)
                break
        self.truncate_file()
        for record in records:
            self.save_object(record)

    def modify_records(self):
        records = list(self.unpickle_database())
        no_of_matching_records = self.get_no_of_matching_objects(records)
        if no_of_matching_records == 1:
            attribute_list = list(records[0].__dict__.keys())[1:]
            print("Modify{}".format(attribute_list))
            to_be_modified_field = input()
            modified_attribute = input('Enter new {}: '.format(to_be_modified_field))
            self.modify_single_record(records, to_be_modified_field, modified_attribute)
        elif no_of_matching_records>1:
            print("These are the records that match your input:\n")
            self.search_records()
            record_id = int(input('Enter id of the record which is to be modified: '))
            attribute_list = list(records[0].__dict__.keys())[1:]
            print("Modify{}".format(attribute_list))
            to_be_modified_field = input()
            modified_attribute = input('Enter new {}: '.format(to_be_modified_field))
            self.modify_record_using_id(records, record_id, to_be_modified_field, modified_attribute)
        elif no_of_matching_records == 0:
            print("No matching records found")