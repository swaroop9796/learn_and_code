from src.db_transactions import *
from src.customer import Customers
from src.employee import Employees


def insert():
    print("Select Category\n1.Customer\n2.Employee")
    choice = int(input())

    if choice is 1:
        name = input("Enter Customer's name: ")
        department = input("Enter Customer department: ")
        customer_obj = Customers(name, department)
        customer_obj.insert_into_file()
        print('1 record inserted')

    elif choice is 2:
        name = input("Enter Employee's name: ")
        project = input("Enter Employee's project: ")
        employee_obj = Employees(name, project)
        employee_obj.insert_into_file()
        print('1 record inserted')


def display():
    print("Select Category\n1.Customer\n2.Employee")
    choice = int(input())
    
    if choice is 1:
        customer_obj = Customers()
        customer_obj.display_all_records()
    
    elif choice is 2:
        employee_obj = Employees()
        employee_obj.display_all_records()

def search():
    print("Select Category\n1.Customer\n2.Employee")
    choice = int(input())
    
    if choice is 1:
        
        customer_name = input('Enter customer name to be searched: ')
        customer_obj = Customers(customer_name)
        flag = customer_obj.search_records()
        if flag is False :
                print('\nNo matching records found\n')
    
    elif choice is 2:
        employee_name = input('Enter Employee name to be searched: ')
        employee_obj = Employees(employee_name)
        flag = employee_obj.search_records()
        if flag is False :
                print('\nNo matching records found\n')

def delete():
    print("Select Category\n1.Customer\n2.Employee")
    choice = int(input())
    
    if choice is 1:
        customer_name = input('Enter customer name to be deleated: ')
        customer_obj = Customers(customer_name)
        result = customer_obj.delete_records()
        if result is False:
            print("1.Delete single record\n2.Delete multiple records\n3.Delete all objects")
            choice = int(input())
            if choice is 1:
                customer_id = int(input("Enter the id of the record to be deleted"))
                customer_obj = Customers(id_num=customer_id)
                customer_obj.delete_single_record()
            elif choice is 2:
                id_list = input("Enter ids to be deleted separated by space").split(' ')
                for val in id_list:
                    customer_obj = Customers(id_num=int(val))
                    customer_obj.delete_single_record()
            elif choice is 3:
                customer_obj.delete_all_records()
    
    elif choice is 2:
        employee_name = input('Enter Employee name to be deleated: ')
        employee_obj = Employees(employee_name)
        result = employee_obj.delete_records()
        if result is False:
            print("1.Delete single record\n2.Delete multiple records\n3.Delete all objects")
            choice = int(input())
            if choice is 1:
                employee_id = int(input("Enter the id of the record to be deleted"))
                employee_obj = Employees(id_num=employee_id)
                employee_obj.delete_single_record()
            elif choice is 2:
                id_list = input("Enter ids to be deleted separated by space:\n").split(' ')
                for val in id_list:
                    employee_obj = Employees(id_num=int(val))
                    employee_obj.delete_single_record()
            elif choice is 3:
                employee_obj.delete_all_records()

def modify():
    print("Select Category\n1.Customer\n2.Employee")
    choice = int(input())

    if choice is 1:
        customer_name = input('Enter customer name to be modified: ')
        customer_obj = Customers(customer_name)
        customer_obj.modify_records()
    elif choice is 2:
        employee_name = input('Enter Employee name to be modified: ')
        employee_obj = Employees(employee_name)
        employee_obj.modify_records()