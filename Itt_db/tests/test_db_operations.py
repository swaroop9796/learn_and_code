import sys
import os
sys.path.append(os.path.dirname(os.getcwd()))
print(os.path.dirname(os.getcwd()))
import unittest
from src.customer import Customers
from src.employee import Employees
import os
import pickle
import sys
import io


class TestDbInsertOperation(unittest.TestCase):

    def setUp(self):
        self.customer_object1 = Customers(name= "Raghavendra", department= 'IS')
        self.customer_object1.insert_into_file()

    def tearDown(self):
        self.dir = os.listdir(os.getcwd())
        for file in self.dir:
            if file.endswith(".txt"):
                os.remove(file)

    def test_if_get_id_returns_proper_id(self):
        file_name = type(self.customer_object1).__name__ + '.txt'
        with open(file_name, 'ab'):
            id = self.customer_object1.get_id()
            self.assertEqual(id, 2)

    def test_insert_operation_with_valid_inputs_by_opening_file(self):
        # self.customer_object1.insert_into_file()
        with open('Customers.txt', 'rb') as output_file:
            record = pickle.load(output_file)
            self.assertEqual(type(self.customer_object1), type(record))
            self.assertEqual(self.customer_object1.name, record.name)
            self.assertEqual(self.customer_object1.department, record.department)
            self.assertEqual(self.customer_object1.id, record.id)

    def test_if_unpickle_method_unpicles_objects(self):
        # self.customer_object1.insert_into_file()
        with open('Customers.txt', 'rb') as output_file:
            list_of_objects = list(self.customer_object1.unpickle_database())
            self.assertEqual(1, len(list_of_objects))

    def test_if_get_id_increments_id_by_1(self):
        self.customer_object1.insert_into_file()
        with open('Customers.txt', 'rb') as output_file:
            id = self.customer_object1.get_id()
            self.assertEqual(id, 3)


class TestDbDisplayAllOperation(unittest.TestCase):

    def setUp(self):
        self.customer_object1 = Customers(name= "Raghavendra", department= 'IS')
        self.customer_object2 = Customers(name="Rahul", department='CS')
        self.employee_object1 = Employees(name="Ravi", project='AI')
        self.customer_object1.insert_into_file()
        self.customer_object2.insert_into_file()

    def tearDown(self):
        self.dir = os.listdir(os.getcwd())
        for file in self.dir:
            if file.endswith(".txt"):
                os.remove(file)

    def test_if_display_all_method_displays_all_records(self):
        capturedOutput = io.StringIO()  # Create StringIO object
        sys.stdout = capturedOutput  # and redirect stdout.
        self.customer_object1.display_all_records()
        sys.stdout = sys.__stdout__  # Reset redirect.
        self.assertEqual(capturedOutput.getvalue(), '1 - Raghavendra - IS\n2 - Rahul - CS\n')

    def test_if_display_all_method_displays_proper_message_when_file_has_no_records(self):
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        self.employee_object1.display_all_records()
        sys.stdout = sys.__stdout__
        self.assertEqual(capturedOutput.getvalue(), 'There is no file associated with Employees object\n')


class TestDbSearchOperation(unittest.TestCase):

    def setUp(self):
        self.customer_object1 = Customers(name= "Raghavendra", department= 'IS')
        self.customer_object2 = Customers(name="Rahul", department='CS')
        self.employee_object1 = Employees(name="Ravi", project='AI')
        self.customer_object1.insert_into_file()

    def tearDown(self):
        self.dir = os.listdir(os.getcwd())
        for file in self.dir:
            if file.endswith(".txt"):
                os.remove(file)

    def test_search_method_returns_True_when_record_found(self):
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        return_value = self.customer_object1.search_records()
        sys.stdout = sys.__stdout__
        self.assertEqual(return_value, True)

    def test_search_method_returns_True_when_record_not_found(self):
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        return_value = self.customer_object2.search_records()
        sys.stdout = sys.__stdout__
        self.assertEqual(return_value, False)

    def test_search_method_returns_True_when_file_not_found(self):
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        self.employee_object1.search_records()
        sys.stdout = sys.__stdout__
        self.assertEqual(capturedOutput.getvalue(), 'There is no file associated with Employees object\n')


class TestDbDeleteOperation(unittest.TestCase):

    def setUp(self):
        self.customer_object1 = Customers(name= "Raghavendra", department= 'IS')
        self.customer_object2 = Customers(name="Raghavendra", department='CS')
        self.customer_object3 = Customers(name="Suresh", department='CS')
        self.employee_object1 = Employees(name="Ravi", project='AI')
        self.employee_object2 = Employees(name="Raju", project='AI')
        self.customer_object1.insert_into_file()
        self.customer_object2.insert_into_file()
        self.customer_object3.insert_into_file()
        self.employee_object1.insert_into_file()
        self.record_objs1 = [self.customer_object1, self.customer_object2]
        self.record_objs2 = [self.customer_object3]

    def tearDown(self):
        self.dir = os.listdir(os.getcwd())
        for file in self.dir:
            if file.endswith(".txt"):
                os.remove(file)

    def test_delete_records_method_deletes_records(self):
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        return_value = self.customer_object3.delete_records()
        sys.stdout = sys.__stdout__
        self.assertEqual(capturedOutput.getvalue(), '1 record deleted\n')
        self.assertEqual(return_value, True)

    def test_delete_records_method_returns_False_when_multiple_records_match(self):
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        return_value = self.customer_object1.delete_records()
        sys.stdout = sys.__stdout__
        self.assertEqual(capturedOutput.getvalue(), 'These are the records that match your input:\n\n1 - Raghavendra - IS\n2 - Raghavendra - CS\n')
        self.assertEqual(return_value, False)

    def test_delete_records_method_returns_True_when_no_records_found(self):
        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        return_value = self.employee_object2.delete_records()
        sys.stdout = sys.__stdout__
        self.assertEqual(capturedOutput.getvalue(), 'No matching records found\n')
        self.assertEqual(return_value, True)

    def test_truncate_file_method_clears_file_data(self):
        self.customer_object1.truncate_file()
        with open('Customers.txt', 'rb') as output_file:
            self.assertRaises(EOFError, pickle.load, output_file)

    def test_save_objects_method_saves_object(self):
        self.employee_object2.save_object(self.employee_object2)
        with open('Employees.txt', 'rb') as output_file:
            for _ in range(2):
                record = pickle.load(output_file)
            self.assertEqual(type(self.employee_object2), type(record))
            self.assertEqual(self.employee_object2.name, record.name)
            self.assertEqual(self.employee_object2.project, record.project)
            self.assertEqual(self.employee_object2.id, record.id)

    def test_get_no_of_matching_objects_returns_correct_number_set1(self):
        obj_count1 = self.customer_object1.get_no_of_matching_objects(self.record_objs1)
        self.assertEqual(obj_count1, 2)

    def test_get_no_of_matching_objects_returns_correct_number_set2(self):
        obj_count2 = self.customer_object3.get_no_of_matching_objects(self.record_objs2)
        self.assertEqual(obj_count2, 1)

    def test_get_no_of_matching_objects_returns_correct_number_set3(self):
        obj_count3 = self.customer_object1.get_no_of_matching_objects(self.record_objs2)
        self.assertEqual(obj_count3, 0)

class TestDbModifyOperation(unittest.TestCase):

    def setUp(self):
        self.customer_object1 = Customers(name= "Raghavendra", department= 'IS')
        self.customer_object2 = Customers(name="Raghavendra", department='CS')
        self.customer_object3 = Customers(name="Suresh", department='CS')
        self.employee_object1 = Employees(name="Ravi", project='AI')
        self.employee_object2 = Employees(name="Raju", project='AI')
        self.customer_object1.insert_into_file()
        self.customer_object2.insert_into_file()
        self.customer_object3.insert_into_file()
        self.employee_object1.insert_into_file()

    def tearDown(self):
        self.dir = os.listdir(os.getcwd())
        for file in self.dir:
            if file.endswith(".txt"):
                os.remove(file)

    def test_modify_single_record_method_modifies_single_record(self):
        self.customer_object3.modify_single_record([self.customer_object1, self.customer_object3], 'name', 'Santhosh')
        self.assertEqual(self.customer_object3.name, 'Santhosh')

    def test_modify_record_using_id_modifies_the_record_property(self):
        self.customer_object1.id = 1
        self.customer_object2.id = 2
        self.customer_object1.modify_record_using_id([self.customer_object1, self.customer_object2], 1,'name', 'Sathish')
        self.assertEqual(self.customer_object1.name, 'Sathish')
