number_of_spiders, number_of_iterations = input().split()
number_of_spiders = int(number_of_spiders)
number_of_iterations = int(number_of_iterations)
power_list = [int(val) for val in input().split()]
power_list = [(power_list[i], i+1) for i in range(number_of_spiders)] #storing powers along with the indeces in a list of tuples

def max_power(list): #function to find max power 
    max = (-1,-1)  #reference power to find max power
    for val in list:  #comparing each power with max and updating max
        if val[0]>max[0]:  
            max = val
    return max

for i in range(number_of_iterations):
    if power_list == None:
        size = 0
    else:
        size = len(power_list)
    if size < number_of_iterations:  #when number of powers in list gows below number_of_iterations
        print(max_power(power_list)[1],end=' ')  #print index of max power in original list
        power_list.remove(max_power(power_list)) #remove max element
        temp_list=[]
        for val in power_list:
            if val[0]>0:
                temp_list.append((val[0]-1, val[1]))  #push the powers other than max by decrementing by 1
            else:
                temp_list.append((val[0], val[1]))  #push the power without decrementing when it reaches 0
        power_list = temp_list.copy()  #copy all values from temporary list to original list
    else: #when number of powers in list is more than number_of_iterations
        popped_list = power_list[ :number_of_iterations] 
        power_list[ :number_of_iterations] = []
        print(max_power(popped_list)[1],end=' ')  #print index of max power in original list
        popped_list.remove(max_power(popped_list))  #remove max element
        for val in popped_list:
            if val[0]>0:
                power_list.append((val[0]-1, val[1])) #push the powers other than max by decrementing by 1
            else:
                power_list.append((val[0], val[1])) #push the power without decrementing when it reaches 0
